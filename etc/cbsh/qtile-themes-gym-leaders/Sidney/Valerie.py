#!/usr/bin/env python3
from libqtile.layout.floating import Floating
from libqtile.config import Match, Rule

class Colors(object):

    black           = ['#3a1e28', '#3a1e28']
    grey            = ['#5F5F64', '#5F5F64']
    white           = ['#ffffff', '#ffffff']
    red             = ['#68617d', '#68617d']
    brightred       = ['#b1a5d5', '#b1a5d5']
    green           = ['#7f4256', '#7f4256']
    brightgreen     = ['#f27ea3', '#f27ea3']
    blue            = ['#776e8f', '#776e8f']
    darkblue        = ['#4cb1ff', '#4cb1ff']
    magenta         = ['#94536d', '#94536d']
    brightmagenta   = ['#f5c1ce', '#f5c1ce']
    cyan            = ['#e5e2a3', '#e5e2a3']
    brightcyan      = ['#f8eea3', '#f8eea3']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"

class Wallpaper(object):
    wallpaper = "~/.config/qtile/themes/Valerie/BG.png"
