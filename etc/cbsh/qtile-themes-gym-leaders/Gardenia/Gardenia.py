#!/usr/bin/env python3
from libqtile.layout.floating import Floating
from libqtile.config import Match, Rule

class Colors(object):

    black           = ['#192916', '#192916']
    grey            = ['#5F5F64', '#5F5F64']
    white           = ['#ffffff', '#ffffff']
    red             = ['#cc4951', '#cc4951']
    brightred       = ['#ea545e', '#ea545e']
    green           = ['#4d7134', '#4d7134']
    brightgreen     = ['#a6d396', '#a6d396']
    blue            = ['#1798cf', '#1798cf']
    darkblue        = ['#1eade3', '#1eade3']
    magenta         = ['#94536d', '#94536d']
    brightmagenta   = ['#f5c1ce', '#f5c1ce']
    cyan            = ['#e5e2a3', '#e5e2a3']
    brightcyan      = ['#f8eea3', '#f8eea3']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"

class Wallpaper(object):
    wallpaper = "~/.config/qtile/themes/Gardenia/BG.png"
