#!/usr/bin/env python3
from libqtile.layout.floating import Floating
from libqtile.config import Match, Rule

class Colors(object):

    black           = ['#322634', '#322634']
    grey            = ['#40394f', '#40394f']
    white           = ['#ffffff', '#ffffff']
    red             = ['#a1505a', '#a1505a']
    brightred       = ['#ef8175', '#ef8175']
    green           = ['#6b395a', '#6b395a']
    brightgreen     = ['#c368a3', '#c368a3']
    blue            = ['#826190', '#826190']
    darkblue        = ['#73557f', '#73557f']
    magenta         = ['#94536d', '#94536d']
    brightmagenta   = ['#f5c1ce', '#f5c1ce']
    cyan            = ['#a4adbc', '#a4adbc']
    brightcyan      = ['#d2def1', '#d2def1']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"

class Wallpaper(object):
    wallpaper = "~/.config/qtile/themes/Shauntal/BG.png"
