#!/usr/bin/env python3

class Colors(object):

    black           = ['#26111e', '#26111e']
    grey            = ['#5F5F64', '#5F5F64']
    white           = ['#ffffff', '#ffffff']
    red             = ['#8d173e', '#8d173e']
    brightred       = ['#bc1e52', '#bc1e52']
    green           = ['#77345e', '#77345e']
    brightgreen     = ['#c368a3', '#c368a3']
    blue            = ['#29618b', '#29618b']
    darkblue        = ['#77c7d4', '#77c7d4']
    magenta         = ['#af370e', '#af370e']
    brightmagenta   = ['#ee6e23', '#ee6e23']
    cyan            = ['#d09c0f', '#d09c0f']
    brightcyan      = ['#fabe12', '#fabe12']


class Fonts(object):
    base = "SauceCodePro Nerd Font"
    bold = "SauceCodePro Nerd Font Bold"


class Wallpaper(object):
    wallpaper = "~/.config/qtile/themes/Roxie/BG.png"
